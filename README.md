# ESPAsyncWebServer Demo

If you want to do any HTTP stuff on ESP32 or ESP8266, you should use ESPAsyncWebServer by me-no-dev.

It is easy to use, super performant and has a loooooooot of useful features.

See https://github.com/me-no-dev/ESPAsyncWebServer.

To run this example:

- Install VSCode
- Install the plugin PlatformIO
- Open this project in code
- Adjust the Wifi access point name and password in the code
- Click the build button at page bottom (the check mark)
- Connect an ESP8266 board to the serial port
- Click upload (the right arrow at page bottom)
- Open the serial monitor by clicking the USB connector symbol at page bottom
- Reset the ESP8266
- You should see connect message and the web address of your ESP8266
- Open the web address in a browser and add /hello to it
- You should see Hello, world!