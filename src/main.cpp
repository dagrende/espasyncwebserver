#include "ESPAsyncTCP.h"
#include "ESPAsyncWebServer.h"

AsyncWebServer server(80);

const char* ssid = "your wifi name";
const char* password = "you wifi pw";

void setup(){
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.printf("WiFi Failed!\n");
    return;
  }
  Serial.print("\nConnected as http://"); Serial.println(WiFi.localIP());

  // serve web requests
  server.on("/hello", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain", "Hello, world!");
  });

  server.begin();
}

void loop(){
}